# Experiments on Dedicated CDCL Strategies for PB Solvers

The purpose of this project is to give access to all experiments performed
to evaluate the implementation of dedicated CDCL strategies in two PB
solvers, namely *Sat4j* and *RoundingSat*.

This is a companion artifact to the paper titled *On Dedicated CDCL Strategies for PB Solvers*
by Daniel Le Berre and Romain Wallon published in SAT 2021 proceedings.

All experiments have been executed on a cluster of computers equipped with quadcore
bi-processors Intel XEON X5550 (2.66 GHz, 8 MB cache) and 32 GB of memory.
The nodes of this cluster run Linux CentOS 7 (x86_64), and use GCC 4.8.5 for
C/C++-based programs and the JDK 11.0.1 for Java-based programs.

The analyses of our experiments can be accessed from the following table of
contents.
All the analyses are powered by [*Metrics*](https://metrics.readthedocs.io/) and
provided as Jupyter Notebooks.
They may thus be reproduced by cloning this repository and running the notebooks again
with the [*latest version of Metrics*](https://pypi.org/project/crillab-metrics/).
Note that, to achieve reproducibility, we also provide the (JSON-serialized) data
we collected during our experiments.

> **Important note:**
> Deserialization issues may occur if your version of *Metrics* is below 1.0.3.
> Please, make sure that you use at least this version before running the analyses.

Also, note that, as we use *Metrics*, some of the vocabulary used by this library
will be used throughout the notebooks below.
In particular, the notion of *experiment-ware* (or *xp-ware*, *ew*) is particularly
important for this library, and represents, in our case, the solvers we are evaluating.
As such, you may simply interpret *experiment-ware* as *solver* in the following
(e.g., a *VBEW* corresponds to a *VBS*).

## Decision Problems

The experiments in this section use as input the whole set of decision
benchmarks containing "small" integers used in the pseudo-Boolean competitions
since the first edition, for a total of 5582 instances.

### RoundingSat

+ [Summary of the Results](roundingsat/roundingsat-overview.ipynb)
+ [Experimental Analysis Focusing on Different Bumping Strategies](roundingsat/roundingsat-bump.ipynb)
+ [Experimental Analysis Focusing on Different Deletion Strategies](roundingsat/roundingsat-delete.ipynb)
+ [Experimental Analysis Focusing on Different Restart Policies](roundingsat/roundingsat-restart.ipynb)
+ [Experimental Analysis Focusing on Different Deletion and Restart Strategies](roundingsat/roundingsat-delete-restart.ipynb)

### Sat4j-GeneralizedResolution

+ [Summary of the Results](sat4j-generalized-resolution/sat4j-generalized-resolution-overview.ipynb)
+ [Experimental Analysis Focusing on Different Bumping Strategies](sat4j-generalized-resolution/sat4j-generalized-resolution-bump.ipynb)
+ [Experimental Analysis Focusing on Different Deletion Strategies](sat4j-generalized-resolution/sat4j-generalized-resolution-delete.ipynb)
+ [Experimental Analysis Focusing on Different Restart Policies](sat4j-generalized-resolution/sat4j-generalized-resolution-restart.ipynb)
+ [Experimental Analysis Focusing on Different Deletion and Restart Strategies](sat4j-generalized-resolution/sat4j-generalized-resolution-delete-restart.ipynb)

### Sat4j-RoundingSat

+ [Summary of the Results](sat4j-roundingsat/sat4j-roundingsat-overview.ipynb)
+ [Experimental Analysis Focusing on Different Bumping Strategies](sat4j-roundingsat/sat4j-roundingsat-bump.ipynb)
+ [Experimental Analysis Focusing on Different Deletion Strategies](sat4j-roundingsat/sat4j-roundingsat-delete.ipynb)
+ [Experimental Analysis Focusing on Different Restart Policies](sat4j-roundingsat/sat4j-roundingsat-restart.ipynb)
+ [Experimental Analysis Focusing on Different Deletion and Restart Strategies](sat4j-roundingsat/sat4j-roundingsat-delete-restart.ipynb)

### Sat4j-PartialRoundingSat

+ [Summary of the Results](sat4j-partial-roundingsat/sat4j-partial-roundingsat-overview.ipynb)
+ [Experimental Analysis Focusing on Different Bumping Strategies](sat4j-partial-roundingsat/sat4j-partial-roundingsat-bump.ipynb)
+ [Experimental Analysis Focusing on Different Deletion Strategies](sat4j-partial-roundingsat/sat4j-partial-roundingsat-delete.ipynb)
+ [Experimental Analysis Focusing on Different Restart Policies](sat4j-partial-roundingsat/sat4j-partial-roundingsat-restart.ipynb)
+ [Experimental Analysis Focusing on Different Deletion and Restart Strategies](sat4j-partial-roundingsat/sat4j-partial-roundingsat-delete-restart.ipynb)

## Optimization Problems

The experiments in this section use as input the whole set of optimization
benchmarks containing "small" integers used in the pseudo-Boolean competitions
since the first edition, for a total of 4374 instances.
For these problems, we only compare the default configurations of *RoundingSat*
and *Sat4j* to the best configurations identified for these solvers in the
previous section.

### RoundingSat

+ [Optimization Problems (Optimum Found/Unsatisfiable Only)](roundingsat-optim/roundingsat-optim.ipynb)
+ [Optimization Problems (Best Found Solutions)](roundingsat-optim/roundingsat-optim-sat.ipynb)

### Sat4j

+ [Optimization Problems (Optimum Found/Unsatisfiable Only)](sat4j-optim/sat4j-optim.ipynb)
+ [Optimization Problems (Best Found Solutions)](sat4j-optim/sat4j-optim-sat.ipynb)
+ [Impact of Irrelevant Literals](irrelevant/sat4j-optim-irrelevant.ipynb)

## All Solvers

In the previous sections, we only compared the different configurations of *RoundingSat*
and *Sat4j* between them.
In this section, we compare these configurations together.

+ [Comparison on Decision Problems](all/decision.ipynb)
+ [Comparison on Optimization Problems](all/optimization.ipynb)
